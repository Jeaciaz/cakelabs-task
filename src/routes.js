import EventList from "./components/EventList";
import EventPage from './components/EventPage';

export default [
	{
		path: '/',
		exact: true,
		component: EventList
	},
	{
		path: '/event/:id',
		component: EventPage
	}
]