import React from 'react';
import './App.scss';
import routes from './routes';
import { Switch, Route } from 'react-router-dom';

function App() {
	const routesJSX = routes.map((route, index) => (
		<Route exact={route.exact} path={route.path} component={route.component} key={index} />
	));

  return (
    <div className="app">
			<header className="app-header">
				<span role="img" aria-label="Cake">🎂</span> CakeEvents
			</header>
			<Switch>
				{ routesJSX }
			</Switch>
    </div>
  );
}

export default App;
