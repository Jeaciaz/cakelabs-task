import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {configureStore} from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import eventListReducer from './slices/eventList';
import { BrowserRouter } from 'react-router-dom';
import './components/styles';

const store = configureStore({
	reducer: {
		eventList: eventListReducer
	}
});

ReactDOM.render((
	<BrowserRouter basename={process.env.PUBLIC_URL}>
		<Provider store={store}>
			<App />
		</Provider>
	</BrowserRouter>
), document.getElementById('root'));

serviceWorker.unregister();
