import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchEvents = () => dispatch => {
	dispatch(fetchEventsStart());
	axios.get('https://api.myjson.com/bins/15oq4e').then(res => {
		dispatch(fetchEventsSuccess(res.data));
	}).catch(res => {
		dispatch(fetchEventsError());
	});
}

const savedFilters = Object.fromEntries(
	Object.keys(localStorage).filter(item => item.startsWith('filter__')).map(filterName => [
		filterName.slice(8), 
		localStorage.getItem(filterName) !== 'false' ? localStorage.getItem(filterName) : ''
	])
);

const eventListSlice = createSlice({
	name: 'eventList',
	initialState: {
		list: [],
		filters: Object.assign({
			categories: '',
			favorite: '',
			title: '',
			description: ''
		}, savedFilters),
		sortParam: localStorage.getItem('sort-param'),
		loading: false
	},
	reducers: {
		fetchEventsStart(state) {
			[state.list, state.loading] = [[], true];
		},
		fetchEventsSuccess(state, { payload }) {
			payload.forEach(event => {
				Object.assign(event, { favorite: localStorage.getItem('favorite__' + event.id) === 'true' })
			});
			[state.list, state.loading] = [payload, false];
		},
		fetchEventsError(state) {
			[state.list, state.loading] = [[], false];
		},
		changeFilter(state, { payload }) {
			state.filters[payload.key] = payload.value;
			localStorage.setItem('filter__' + payload.key, payload.value);
		},
		toggleFavorite(state, { payload }) {
			const event = state.list.find(event => event.id === payload);
			event.favorite = !event.favorite;
			localStorage.setItem('favorite__' + event.id, event.favorite);
		},
		setSortParam(state, {payload}) {
			state.sortParam = payload;
			localStorage.setItem('sort-param', payload);
		}
	}
});

export const {
	fetchEventsStart,
	fetchEventsSuccess,
	fetchEventsError,
	changeFilter,
	toggleFavorite,
	setSortParam
} = eventListSlice.actions;

export default eventListSlice.reducer;

export function getFilteredList(state) {
	const filteredList = Object.keys(state.filters).reduce((accum, filterName) => accum.filter(item => {
		const value = item[filterName]
		// Have to differentiate between filtering arrays and individual values
		// Deep arrays and objects are as of now unfilterable (or badly filterable)
		if (Array.isArray(value)) {
			return String(value.join('')).includes(state.filters[filterName]);
		} else {
			return String(value).includes(state.filters[filterName]);
		}
	}), state.list);
	
	return {
		...state,
		list: filteredList
	};
}

export function getSortedList(state) {
	const sortedList = state.sortParam 
		? state.list.slice().sort((a, b) => !a[state.sortParam] || parseFloat(a[state.sortParam]) < parseFloat(b[state.sortParam]) ? -1 : 1) 
		: state.list.slice();

	return {
		...state,
		list: sortedList
	};
}
