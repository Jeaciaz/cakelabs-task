import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchEvents } from '../slices/eventList';
import Loader from './Loader';
import { Link } from 'react-router-dom';

export default function EventPage(props) {

	const event = useSelector(state => state.eventList.list.find(item => String(item.id) === props.match.params.id));
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchEvents());
	}, [dispatch]);

	return event ? (
		<main className="event-page">
			<h1>{ event.title }</h1>
			<p>{ event.description }</p>
			<Link className="event-page__link" to="/">&lt;&lt; Назад</Link>
		</main>
	) : <Loader />
}
