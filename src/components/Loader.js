import React from 'react';
import loader from '../assets/loader.svg';

export default function Loader() {
	return (
		<img className="loader" src={loader} alt="" />
	)
}
