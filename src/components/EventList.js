import React, { useEffect } from 'react';
import Event from './Event';
import Loader from './Loader';
import { useSelector, useDispatch } from 'react-redux';
import { fetchEvents, getFilteredList, changeFilter, getSortedList, setSortParam } from '../slices/eventList';

export default function EventList() {
	const eventList = useSelector(state => getFilteredList(getSortedList(state.eventList)).list);
	const isLoading = useSelector(state => state.eventList.loading);
	const isSortedByPrice = useSelector(state => state.eventList.sortParam === 'price')

	const filters = useSelector(state => state.eventList.filters);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchEvents());
	}, [dispatch]);

	const events = eventList.map(event => (
		<Event key={event.id} event={event} />
	));

	return isLoading ? <Loader /> : (
		<main className="event-list">
			<div className="event-list__filters">
				<input className="event-list__text-filter" type="text" value={filters.title} onChange={e => dispatch(changeFilter({key: 'title', value: e.target.value}))} placeholder="Название события..." />
				<input className="event-list__text-filter" type="text" value={filters.description} onChange={e => dispatch(changeFilter({key: 'description', value: e.target.value}))} placeholder="Описание..." />
				<input className="event-list__text-filter" type="text" value={filters.categories} onChange={e => dispatch(changeFilter({key: 'categories', value: e.target.value}))} placeholder="Название категории..." />
				<label className="event-list__checkbox-filter">
					<input 
						type="checkbox" 
						checked={filters.favorite} 
						onChange={e => dispatch(changeFilter({key: 'favorite', value: e.target.checked || ''}))} 
					/> Только избранное
				</label>
				{/* Можно сделать выбор параметра сортировки, но нужно ли в рамках тестового задания? */}
				<label className="event-list__checkbox-filter">
					<input 
						type="checkbox" 
						checked={isSortedByPrice}
						onChange={e => e.target.checked ? dispatch(setSortParam('price')) : dispatch(setSortParam(''))} 
					/> Сортировать по цене
				</label>
			</div>
			{ events }
		</main>
	)
}
