import React from 'react';
import { changeFilter, toggleFavorite } from '../slices/eventList';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

export default function Event(props) {
	const dispatch = useDispatch();

	const categories = props.event.categories.map((category, index) => (
		<li 
			key={index} 
			className="event-card__category" 
			onClick={e => dispatch(changeFilter({
				key: 'categories', 
				value: e.target.textContent
			}))}
		>{ category }</li>
	));

	return (
		<div className="event-card">
			<Link className="event-card__title" to={`/event/${props.event.id}`}>{ props.event.title }</Link>
			<h4 className="event-card__price">{ props.event.price ? '$' + props.event.price : 'Бесплатно' }</h4>
			<p className="event-card__description">{ props.event.description }</p>
			<button className="event-card__action" onClick={() => dispatch(toggleFavorite(props.event.id))}>{props.event.favorite ? '★ Убрать из избранного' : '☆ Добавить в избранное'}</button>
			<ul className="event-card__category-list">
				{ categories }
			</ul>
		</div>
	)
}
